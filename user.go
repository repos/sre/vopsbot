package main

/*
Copyright (c) 2022- Giuseppe Lavagetto, Wikimedia Foundation Inc

This code is released under the GNU General Public license, version
3 or later. See LICENSE and AUTHORS for details

**/

import (
	"os"

	"github.com/lavagetto/ircbot/ircbot"
	"gopkg.in/yaml.v3"
)

// Structure for a user
type User struct {
	// VOName is the name of the user on victorops
	VOName string `yaml:"vo_name"`
	// Teams are the teams the user is part of
	Teams []string `yaml:"teams"`
	// The user is a victorops admin
	VOAdmin bool `yaml:"vo_admin"`
}

func (u *User) IsSRE() bool {
	return contains(u.Teams, sreTeam)
}

// Map of irc nicknames => vo properties
var userMap = make(map[string]User)

// Fetch user overrides from file
func GetUsersFromFile(filepath string) {
	f, err := os.ReadFile(filepath)
	if err != nil {
		panic(err)
	}
	err = yaml.Unmarshal(f, &userMap)
	if err != nil {
		panic(err)
	}
}

// Given a victorops username, get the corresponding IRC nick
// If none is found, an empty string is returned.
func findIrcNick(voname string) string {
	for ircnick, user := range userMap {
		if user.VOName == voname {
			return ircnick
		}
	}
	return ""
}

// AddAdmins adds all the members of the SRE team to the admins list.
func AddAdmins(userConfigPath string, irc *ircbot.IrcBot, v *VOActions) {
	// Load ircnick overrides/configuration from a file.
	// Please not that most of the content of the file will be overridden later.
	// It makes sense to bootstrap a full user list in case we have issues with
	// the api
	if userConfigPath != "" {
		GetUsersFromFile(userConfigPath)
	}
	err := v.GetUsersFromApi(irc)
	if err != nil {
		irc.Logger().Error("Could not load the users from the victorops api", "error", err)
	}
	c := irc.Config()
	// All users in the SRE team are added as admins of the bot by default.
	for ircName, user := range userMap {
		if user.IsSRE() && !contains(c.Admins, ircName) {
			irc.Logger().Info("adding SRE to admins", "ircname", ircName)
			c.Admins = append(c.Admins, ircName)
		}
	}
}
