# VOPSBOT

This is an IRC bot with some basic functionality for interacting with victorops.

It uses the [ircbot framework](https://github.com/lavagetto/ircbot) and at the moment it can do the following things:
* List who's oncall
* List the paging incidents of the last 24 hours
* Automatically refresh the topic of a channel with the currently oncall SREs
* Acknowledge/resolve an incident

## Usage
Before you start, you need to initialize an sqlite3 database:

```bash
   $ sqlite3 vopsbot.db < schema.sql
```

You need to provide 3 environment variables:
* VO_API_ID your victrorops api ID
* VO_API_KEY an associated api key
* CONFIG the path to the configuration file for the ircbot framework.
  See the [ircbot documentation](https://github.com/lavagetto/ircbot/blob/main/README.md#running-ircbot) for the format.

Once the bot is started your named admins need to configure [ACLs](https://github.com/lavagetto/ircbot/blob/main/README.md#acls) for all commands.

Find more details about the commands by running `!help` in any channel where the bot is present.

## How to build the debian package

Packages will be automatically built when you make a change to `debian/changelog` and that is pushed to master. Read [the details on wikitech](https://wikitech.wikimedia.org/wiki/Debian_packaging/Package_your_software_as_deb).