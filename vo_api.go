package main

import (
	"database/sql"
	"fmt"
	"regexp"
	"strconv"
	"strings"
	"time"

	log "gopkg.in/inconshreveable/log15.v2"

	"github.com/lavagetto/go-victorops/victorops"
	"github.com/lavagetto/ircbot/ircbot"
	"github.com/lavagetto/ircbot/utils"
	hbot "github.com/whyrusleeping/hellabot"
)

/*
Copyright (c) 2022- Giuseppe Lavagetto, Wikimedia Foundation Inc

This code is released under the GNU General Public license, version
3 or later. See LICENSE and AUTHORS for details

**/

// This file contains the class to interact with the Victorops API

// Container for actions
type VOActions struct {
	client *victorops.Client
	logger log.Logger
}

const MAX_ONCALL = 4

var noPingUser = regexp.MustCompile(`(\w)(.*)`)
var topicRegex = regexp.MustCompile(`^(.*)\| SREs on call: ([^\|]+)(.*)$`)
var topicClinicRegex = regexp.MustCompile(`^(.*)\| SRE Clinic Duty: ([^\|]+)(.*)$`)

// VictoropsApi initializes the victorops api client
// And also fills in the escalation policies list.
func VictorOpsApi(ID string, Key string, irc *ircbot.IrcBot) *VOActions {
	vo := &VOActions{client: victorops.NewClient(ID, Key, "https://api.victorops.com"), logger: irc.Logger()}
	vo.getEscPolicies(irc, false)
	return vo
}

func (v *VOActions) getEscPolicies(irc *ircbot.IrcBot, force bool) {
	if len(escalationMap) > 0 && !force {
		return
	}
	policies, details, err := v.client.GetAllEscalationPolicies()
	if err = apiError(details, err); err != nil {
		irc.Logger().Error("Error fetching escalation policies", "error", err)
		return
	}
	for _, policy := range policies.Policies {
		teamName := strings.ToLower(policy.Team.Name)
		policyName := strings.ToLower(policy.Policy.Name)
		policyName = strings.Replace(policyName, teamName, "", -1)
		policyName = strings.Replace(policyName, " (escalation)", "", -1)
		policyName = strings.Replace(policyName, " (non-paging)", "", -1)
		policyName = strings.Trim(policyName, " ")
		policyName = strings.Replace(policyName, " ", "_", -1)
		if escalationMap[teamName] == nil {
			escalationMap[teamName] = make(map[string]string)
		}
		escalationMap[teamName][policyName] = policy.Policy.Slug
	}
}

func (v *VOActions) getTeams(m *hbot.Message, irc *ircbot.IrcBot) *[]victorops.Team {
	teams, _, err := v.client.GetAllTeams()
	if err != nil {
		irc.Reply(m, "Could not fetch teams from the api, sorry")
		irc.Logger().Error("Error fetching teams", err)
		return nil
	}
	return teams
}

func (v *VOActions) getTeamByName(team string, m *hbot.Message, irc *ircbot.IrcBot) *victorops.Team {
	teams := v.getTeams(m, irc)
	if teams == nil {
		return nil
	}
	for _, t := range *teams {
		if t.Name == team {
			return &t
		}
	}
	irc.Reply(m, fmt.Sprintf("team %s not found", team))
	return nil
}

func (v *VOActions) oncallFor(team string, escalation string) ([]string, error) {
	resp := make([]string, 0, 2)
	teams, det, err := v.client.GetOnCallCurrent()
	if err != nil {
		return resp, err
	}
	if det.StatusCode != 200 {
		return resp, fmt.Errorf("got an api response with status %d", det.StatusCode)
	}
	for _, teamOnCall := range teams.TeamsOnCall {
		if strings.ToLower(teamOnCall.Team.Name) == team {
			for _, rotation := range teamOnCall.OnCallNow {
				if rotation.EscalationPolicy.Slug == escalation {
					for _, u := range rotation.Users {
						// devnull is not a real user ;)
						if u.OnCallUser.Username == "devnull" {
							continue
						}
						ircnick := findIrcNick(u.OnCallUser.Username)

						if ircnick == "" {
							v.logger.Info("translation not found for user", "user", u.OnCallUser.Username)
							ircnick = u.OnCallUser.Username
						}
						resp = append(resp, ircnick)
					}
				}
			}
		}
	}
	return resp, nil
}

// Finds who is currently oncall for a team/rotation
func (v *VOActions) WhoIsOnCall(args map[string]string, m *hbot.Message, irc *ircbot.IrcBot) bool {
	v.getEscPolicies(irc, false)
	team := strings.ToLower(args["team"])
	teamEscalations, ok := escalationMap[team]
	if !ok {
		irc.Reply(m, fmt.Sprintf("Could not find the desired team '%s'.", team))
		return true
	}
	// This might seem redundant, but it's needed for properly managing the SRE override.
	escalations := make([]string, 0, len(teamEscalations))
	for esc := range teamEscalations {
		escalations = append(escalations, esc)
	}
	if len(escalations) == 0 {
		irc.Reply(m, fmt.Sprintf("Didn't find any escalation policies for team '%s'", team))
	}
	// SRE override.
	// There is frankly no way to do this that is not going to assume
	// a lot of implementation-specific things, and I don't think
	// it would be better tbh.
	if team == "sre" {
		escalations = []string{rotation, batphone}
	}
	// Detect if we're in a public channel; in that case, try not to bother the poor oncall people
	// that might be in the room.
	// Countless curses directed my way will be avoided thanks to this trick. Kudos to rzl for
	// suggesting it
	isPublic := strings.HasPrefix(m.To, "#")
	for _, escalation := range escalations {
		users, err := v.oncallFor(team, teamEscalations[escalation])
		if err != nil {
			irc.Reply(m, "error fetching current oncall user.")
			irc.Logger().Error("error", err)
			return true
		}
		if len(users) != 0 {
			to_print := strings.Join(users, ", ")
			if isPublic {
				nonPingUsers := make([]string, len(users))
				for i, user := range users {
					nonPingUsers[i] = noPingUser.ReplaceAllString(user, `$1.$2`)
				}
				to_print = strings.Join(nonPingUsers, ", ")
			}
			irc.Reply(m, fmt.Sprintf("Oncall now for team %s, rotation %s:", args["team"], escalation))
			irc.Reply(m, to_print)
			return true
		}
	}
	irc.Reply(m, fmt.Sprintf("No one is oncall for team %s. Something is *very* wrong 🤔", args["team"]))
	return true
}

func isTopicUpdated(channel string, newTopic string, db *sql.DB) error {
	// check that the topic actually changes.
	// if it does, the handler saving changed topics should eventually catch up
	for i := 0; i < 2; i++ {
		time.Sleep(1 * time.Second)
		t := utils.NewTopic(db, channel)
		topic, err := t.Get()
		if err == nil && topic == newTopic {
			return nil
		}
	}
	return fmt.Errorf("it seems we were unable to update the topic for channel %s", channel)
}

// Adds or modifies information in a topic, returns it modified
func formatTopic(topic string, re *regexp.Regexp, format string, slug string) string {
	formatted := fmt.Sprintf(format, slug)
	matches := re.FindStringSubmatch(topic)
	// The regex is not found. We need to append our text.
	if matches == nil {
		return fmt.Sprintf("%s | %s", topic, formatted)
	} else {
		if matches[3] != "" {
			// Add Padding if we have trailing content in the topic
			formatted += " "
		}
		if slug != matches[2] {
			return fmt.Sprintf("%s| %s%s", matches[1], formatted, matches[3])
		} else {
			return topic
		}
	}
}

// Refreshes the topic for a channel. This is supposed to be running in a separate goroutine.
func (v *VOActions) RefreshTopic(channel string, irc *ircbot.IrcBot, oncall, onclinic []string) bool {
	topic, err := utils.NewTopic(irc.DB(), channel).Get()
	if err != nil {
		irc.Logger().Error("could not find the topic for this channel stored. Is the bot in the channel?", "error", err)
		return false
	}
	if len(oncall) == 0 {
		oncall = append(oncall, "batphone")
	}
	// If for some reason more than normal oncaller limit is reached, let's not ping everyone
	if len(oncall) > MAX_ONCALL {
		oncall = []string{"batphone"}
	}
	if len(onclinic) == 0 {
		onclinic = append(onclinic, "no one")
	}
	// Now update the topic
	oncallSlug := strings.Join(oncall, ", ")
	onclinicSlug := strings.Join(onclinic, ", ")
	newTopic := formatTopic(topic, topicRegex, "SREs on call: %s", oncallSlug)
	newTopic = formatTopic(newTopic, topicClinicRegex, "SRE Clinic Duty: %s", onclinicSlug)
	if newTopic != topic {
		irc.Msg("ChanServ", fmt.Sprintf("TOPIC %s %s", channel, newTopic))
		err = isTopicUpdated(channel, newTopic, irc.DB())
		if err != nil {
			irc.Logger().Error("could not update topic", "error", err)
			return false
		}
		irc.Logger().Info("topic updated", "channel", channel, "topic", newTopic)
		return true
	} else {
		irc.Logger().Info("topic already up to date", "channel", channel)
		return true
	}

}

func fmtIncident(inc victorops.Incident) string {
	return fmt.Sprintf("%s (%s) %s %s", inc.IncidentNumber, inc.CurrentPhase, inc.Host, strings.Replace(inc.Service, "#page", "(paged)", -1))
}

// Display the unresolved paging incidents for the last 24 hours
func (v *VOActions) GetIncidents(args map[string]string, m *hbot.Message, irc *ircbot.IrcBot) bool {
	team := v.getTeamByName(args["team"], m, irc)
	if team == nil {
		irc.Reply(m, "could not find the team")
		return true
	}
	incidents, det, err := v.client.GetIncidents()
	if err != nil {
		irc.Logger().Error("Error fetching incidents", "error", err)
		return true
	}
	if det.StatusCode != 200 {
		irc.Logger().Error("Bad response from incidents api", "code", det.StatusCode)
		return true
	}
	nIncidents := 0
	for _, inc := range incidents.Incidents {
		// Print out only incidents from the last 24 hours that paged the selected team
		if contains(inc.PagedTeams, team.Slug) && time.Since(inc.StartTime) < time.Duration(24)*time.Hour {
			nIncidents++
			irc.Reply(m, fmtIncident(inc))
		}
	}
	if nIncidents == 0 {
		irc.Reply(m, fmt.Sprintf("No incidents occurred in the past 24 hours for team %s", team.Name))
	}
	return true
}

func (v *VOActions) GetUsersFromApi(irc *ircbot.IrcBot) error {
	teams, details, err := v.client.GetAllTeams()
	if err := apiError(details, err); err != nil {
		return fmt.Errorf("getallteams: %v", err)
	}
	for _, team := range *teams {
		irc.Logger().Debug("Working on team", "name", team.Name)
		members, details, err := v.client.GetTeamMembers(team.Slug)
		if err := apiError(details, err); err != nil {
			return fmt.Errorf("getteammembers[%s]: %v", team.Name, err)
		}
		for _, member := range members.Members {
			irc.Logger().Debug("Found team member", "name", member.Username)
			// First let's check if the user is already in the map under a different name than the victorops one
			ircNick := findIrcNick(member.Username)
			if ircNick == "" {
				ircNick = member.Username
			}
			if u, ok := userMap[ircNick]; ok {
				irc.Logger().Debug("Adding user to team", "user", u.VOName, "team", team.Name)
				u.Teams = append(u.Teams, team.Name)
				userMap[ircNick] = u
			} else {
				irc.Logger().Debug("Creating user with team", "user", member.Username, "team", team.Name)
				voUser := User{
					VOName:  member.Username,
					VOAdmin: member.Admin,
					Teams:   []string{team.Name},
				}
				userMap[ircNick] = voUser
			}
		}
	}
	return nil
}

// Was it really too hard to add something like this to the standard library, Rob?
func contains(haystack []string, needle string) bool {
	for _, el := range haystack {
		if el == needle {
			return true
		}
	}
	return false
}

func (v *VOActions) manageIncident(what string, args map[string]string, m *hbot.Message, irc *ircbot.IrcBot) bool {
	incidentId, err := strconv.Atoi(args["incident"])
	if err != nil {
		irc.Reply(m, "Incident id must be an integer")
		return true
	}
	username := args["username"]
	var resp *victorops.IncidentActionResponse
	var details *victorops.RequestDetails
	if what == "ack" {
		resp, details, err = v.client.AckIncidents(username, []int{incidentId}, "Acked via vopsbot")
	} else if what == "resolve" {
		resp, details, err = v.client.ResolveIncidents(username, []int{incidentId}, "Acked via vopsbot")
	} else {
		return true
	}

	err = apiError(details, err)
	if err != nil {
		irc.Logger().Error("could not act on an incident", "username", username, "id", incidentId, "error", err)
		irc.Reply(m, fmt.Sprintf("Could not %s the alert. Please check the parameters.", what))
		return true
	} else if resp.Results[0].CmdAccepted {
		inc, details, err := v.client.GetIncident(incidentId)
		if err = apiError(details, err); err != nil {
			irc.Reply(m, fmt.Sprintf("Incident %d %sed, but could not fetch it again from the API", incidentId, what))
		} else {
			irc.Reply(m, fmtIncident(*inc))
		}
	} else {
		irc.Reply(m, fmt.Sprintf("Attempt to %s incident %d failed.", what, incidentId))
	}
	// Need the method implemented in the library.
	return true
}

// Acknowledge a specific incident
func (v *VOActions) AckIncident(args map[string]string, m *hbot.Message, irc *ircbot.IrcBot) bool {
	return v.manageIncident("ack", args, m, irc)
}

// Resolve a specific incident
func (v *VOActions) ResolveIncident(args map[string]string, m *hbot.Message, irc *ircbot.IrcBot) bool {
	return v.manageIncident("resolve", args, m, irc)
}
