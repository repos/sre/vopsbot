package main

import (
	"context"
	"fmt"
	"os"
	"os/signal"
	"syscall"
	"time"

	"github.com/lavagetto/go-victorops/victorops"

	"github.com/lavagetto/ircbot/ircbot"
	hbot "github.com/whyrusleeping/hellabot"
	log "gopkg.in/inconshreveable/log15.v2"
)

const batphone = "batphone"
const rotation = "business_hours"
const sreTeam = "SRE"

// This is a map of team -> rotation -> policy slug
// Can only be initialized from the VO api.
var escalationMap = make(map[string]map[string]string)

func apiError(details *victorops.RequestDetails, err error) error {
	if err != nil {
		return err
	}
	if details.StatusCode != 200 {
		return fmt.Errorf("got a non-200 response %d", details.StatusCode)
	}
	return nil
}

// Guess which team the user wants.
func guessTeam(m *hbot.Message) string {
	if user, ok := userMap[m.From]; ok {
		// We'll get ths first team, unless SRE is included
		if user.IsSRE() {
			return sreTeam
		}
		return user.Teams[0]
	}
	// Team not found. Assume they want
	// to know about SREs.
	return sreTeam
}

// Guess which username the user wants.
func guessUsername(m *hbot.Message) string {
	if user, ok := userMap[m.From]; ok {
		return user.VOName
	}
	// Else return the empty string. This will cause an error.
	return ""
}

func main() {
	// Initialize the IRC bot framework
	irc, err := ircbot.Init(os.Getenv("CONFIG"))
	if err != nil {
		panic(err)
	}
	if os.Getenv("DEBUG") != "" {
		irc.Logger().SetHandler(log.LvlFilterHandler(log.LvlDebug, log.StdoutHandler))
	}
	// Initialize the Victorops api client.
	voApiID := os.Getenv("VO_API_ID")
	voApiKey := os.Getenv("VO_API_KEY")
	vo := VictorOpsApi(voApiID, voApiKey, irc)

	// Initalize the users configuration, add SREs to the admins list.
	usersConfig := os.Getenv("USERS_FILE")
	AddAdmins(usersConfig, irc, vo)

	// COMMAND: oncall_now
	// Find out who is oncall for a specific rotation.
	oncall := irc.AddCommand("oncall-now", vo.WhoIsOnCall).SetHelp("Find out who is oncall for a team").AllowChannel().AllowPrivate()
	oncall.AddParameterWithDefaultCb("team", `\w+`, guessTeam)

	// COMMAND: incidents
	// Get all current incidents
	irc.AddCommand("incidents", vo.GetIncidents).SetHelp("Display a list of current incidents").AllowChannel().AddParameterWithDefaultCb("team", `^\S+$`, guessTeam)

	// COMMANDS: incident_ack/incident_resolve
	// Ack an incident
	irc.AddCommand("ack", vo.AckIncident).SetHelp("Acknowledge an incident").AllowChannel().AddParameter("incident", `^\d+$`).AddParameterWithDefaultCb("username", `^\S+$`, guessUsername)
	// Ack an incident
	irc.AddCommand("resolve", vo.ResolveIncident).SetHelp("Resolve an incident").AllowChannel().AddParameter("incident", `^\d+$`).AddParameterWithDefaultCb("username", `^\S+$`, guessUsername)
	// Set up the basic signal handling
	ctx, cancelFunc := context.WithCancel(context.Background())

	// Spawn a goroutine to run the topic refresh
	go func() {
		refreshTopic(vo, irc, ctx)
	}()
	// Now run the irc server.
	// We run it in a goroutine so it doesn't block the
	// execution of the main thread. It handles signals by
	// itself.
	go func() { irc.Run() }()
	done := make(chan os.Signal, 1)
	signal.Notify(done, os.Interrupt, syscall.SIGINT, syscall.SIGTERM)
	s := <-done
	irc.Logger().Info("shutting down", "signal", s.String())
	cancelFunc()
	irc.Close()
}

// Update the topic every 5 minutes
func refreshTopic(v *VOActions, irc *ircbot.IrcBot, ctx context.Context) {
	ticker := time.NewTicker(5 * time.Minute)
	for {
		select {
		case <-ctx.Done():
			ticker.Stop()
			return
		case <-ticker.C:
			oncall, err := v.oncallFor("sre", escalationMap["sre"][rotation])
			if err != nil {
				irc.Logger().Error("could not find who's currently oncall", "error", err)
				continue
			}
			clinic, err := v.oncallFor("sre", escalationMap["sre"]["clinic_duty"])
			if err != nil {
				irc.Logger().Error("could not find who's currently on clinic duty", "error", err)
				continue
			}
			maxFail := len(irc.Config().Channels)
			failed := 0
			for _, channel := range irc.Config().Channels {
				if !v.RefreshTopic(channel, irc, oncall, clinic) {
					failed++
				}
			}
			if failed >= maxFail && maxFail > 0 {
				panic("Could not update any topic, something is likely wrong.")
			}
		}
	}
}
