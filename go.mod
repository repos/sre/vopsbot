module gitlab.wikimedia.org/repos/sre/vopsbot

go 1.18

require (
	github.com/lavagetto/go-victorops v1.0.8
	github.com/lavagetto/ircbot v0.2.6
	github.com/whyrusleeping/hellabot v0.0.0-20220131094808-3d595078da57
	gopkg.in/inconshreveable/log15.v2 v2.0.0-20200109203555-b30bc20e4fd1
	gopkg.in/yaml.v3 v3.0.1
)

require (
	github.com/ftrvxmtrx/fd v0.0.0-20150925145434-c6d800382fff // indirect
	github.com/go-stack/stack v1.8.0 // indirect
	github.com/inconshreveable/log15 v0.0.0-20200109203555-b30bc20e4fd1 // indirect
	github.com/mattn/go-colorable v0.1.6 // indirect
	github.com/mattn/go-isatty v0.0.12 // indirect
	github.com/mattn/go-sqlite3 v1.14.14 // indirect
	golang.org/x/sys v0.0.0-20220624220833-87e55d714810 // indirect
	gopkg.in/sorcix/irc.v2 v2.0.0-20200812151606-3f15758ea8c7 // indirect
)
